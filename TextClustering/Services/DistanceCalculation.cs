﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TextClustering.Services
{
    public class DistanceCalculation
    {
        public static float FindDotProduct(float[] vecA, float[] vecB)
        {

            float dotProduct = 0;
            for (var i = 0; i < vecA.Length; i++)
            {
                dotProduct += (vecA[i] * vecB[i]);
            }

            return dotProduct;
        }
        public static float FindMagnitude(float[] vector)
        {
            return (float)Math.Sqrt(FindDotProduct(vector, vector));
        }

        public static float FindCosineSimilarity(float[] vecA, float[] vecB)
        {
            var dotProduct = FindDotProduct(vecA, vecB);
            var magnitudeOfA = FindMagnitude(vecA);
            var magnitudeOfB = FindMagnitude(vecB);
            float result = dotProduct / (magnitudeOfA * magnitudeOfB);
            //when 0 is divided by 0 it shows result NaN so return 0 in such case.
            if (float.IsNaN(result))
                return 0;
            else
                return (float)result;
        }
        public static float FindCosineDistance(float[] vecA, float[] vecB)
        {
            float result = 1.0F - FindCosineSimilarity(vecA, vecB);
            return result;
        }

    }
}
