﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TextClustering.Models;

namespace TextClustering.Services
{
    public class KMean
    {
        private static int globalCounter = 0;
        private static int counter;
        /// <summary>
        /// Prepares the document cluster, Grouping of similar 
        /// type of text document is done here
        /// </summary>
        /// <param name="k">initial cluster center</param>
        /// <param name="documentCollection">document corpus</param>
        /// <returns></returns>


        public static List<DocumentObjModel> PrepareDocumentCluster(int k, List<DocumentVectorSpaceModel> documentCollection, ref int _counter, ref float totalMean)
        {
            globalCounter = 0;
            //prepares k initial centroid and assign one object randomly to each centroid
            List<DocumentObjModel> centroidCollection = new List<DocumentObjModel>();
            DocumentObjModel c;

            /*
             * Avoid repeation of random number, if same no is generated more than once same document is added to the next cluster 
             * so avoid it using HasSet collection
             */
            HashSet<int> uniqRand = new HashSet<int>();
            GenerateRandomNumber(ref uniqRand, k, documentCollection.Count);

            foreach (int pos in uniqRand)
            {
                c = new DocumentObjModel();
                c.GroupedDocument = new List<DocumentVectorSpaceModel>();
                c.GroupedDocument.Add(documentCollection[pos]);
                centroidCollection.Add(c);
            }

            Boolean stoppingCriteria;
            List<DocumentObjModel> resultSet;
            List<DocumentObjModel> prevClusterCenter;

            InitializeClusterCentroid(out resultSet, centroidCollection.Count);

            do
            {
                prevClusterCenter = centroidCollection;

                foreach (DocumentVectorSpaceModel obj in documentCollection)
                {
                    int index = FindClosestClusterCenter(centroidCollection, obj);
                    resultSet[index].GroupedDocument.Add(obj);
                }
                InitializeClusterCentroid(out centroidCollection, centroidCollection.Count());
                centroidCollection = CalculateMeanPoints(resultSet);
                stoppingCriteria = CheckStoppingCriteria(prevClusterCenter, centroidCollection);
                if (!stoppingCriteria)
                {
                    //initialize the result set for next iteration
                    InitializeClusterCentroid(out resultSet, centroidCollection.Count);
                }
                counter++;

            } while (stoppingCriteria == false);

            _counter = counter;

            foreach (DocumentObjModel cluster in resultSet)
            {
                cluster.TotalValue = CalculateMeanPointsOfCluster(cluster);
                totalMean += cluster.TotalValue;
            }
            totalMean /= k;
            return resultSet;

        }

        /// <summary>
        /// Generates unique random numbers and also ensures the generated random number 
        /// lies with in a range of total no. of document
        /// </summary>
        /// <param name="uniqRand"></param>
        /// <param name="k"></param>
        /// <param name="docCount"></param>

        private static void GenerateRandomNumber(ref HashSet<int> uniqRand, int k, int docCount)
        {

            Random r = new Random();

            if (k > docCount)
            {
                do
                {
                    int pos = r.Next(0, docCount);
                    uniqRand.Add(pos);

                } while (uniqRand.Count != docCount);
            }
            else
            {
                do
                {
                    int pos = r.Next(0, docCount);
                    uniqRand.Add(pos);

                } while (uniqRand.Count != k);
            }
        }

        /// <summary>
        /// Initialize the result cluster centroid for the next iteration, that holds the result to be returned
        /// </summary>
        /// <param name="centroid"></param>
        /// <param name="count"></param>
        private static void InitializeClusterCentroid(out List<DocumentObjModel> centroid, int count)
        {
            DocumentObjModel c;
            centroid = new List<DocumentObjModel>();
            for (int i = 0; i < count; i++)
            {
                c = new DocumentObjModel();
                c.GroupedDocument = new List<DocumentVectorSpaceModel>();
                centroid.Add(c);
            }

        }

        /// <summary>
        /// Check the stopping criteria for the iteration, if centroid do not move their position it meets the criteria
        /// or if the global counter exist its predefined limit(minimum iteration threshold) than iteration terminates
        /// </summary>
        /// <param name="prevClusterCenter"></param>
        /// <param name="newClusterCenter"></param>
        /// <returns></returns>
        private static Boolean CheckStoppingCriteria(List<DocumentObjModel> prevClusterCenter, List<DocumentObjModel> newClusterCenter)
        {

            globalCounter++;
            counter = globalCounter;
            if (globalCounter > 11000)
            {
                return true;
            }

            else
            {
                Boolean stoppingCriteria;
                int[] changeIndex = new int[newClusterCenter.Count()]; //1 = centroid has moved 0 == centroid do not moved its position

                int index = 0;
                do
                {
                    int count = 0;
                    if (newClusterCenter[index].GroupedDocument.Count == 0 && prevClusterCenter[index].GroupedDocument.Count == 0)
                    {
                        index++;
                    }
                    else if (newClusterCenter[index].GroupedDocument.Count != 0 && prevClusterCenter[index].GroupedDocument.Count != 0)
                    {
                        for (int j = 0; j < newClusterCenter[index].GroupedDocument[0].VectorSpace.Count(); j++)
                        {
                            //
                            if (newClusterCenter[index].GroupedDocument[0].VectorSpace[j] == prevClusterCenter[index].GroupedDocument[0].VectorSpace[j])
                            {
                                count++;
                            }

                        }

                        if (count == newClusterCenter[index].GroupedDocument[0].VectorSpace.Count())
                        {
                            changeIndex[index] = 0;
                        }
                        else
                        {
                            changeIndex[index] = 1;
                        }
                        index++;
                    }
                    else
                    {
                        index++;
                        continue;

                    }


                } while (index < newClusterCenter.Count());

                // if index list contains 1 stopping criteria is set to flase
                if (changeIndex.Where(s => (s != 0)).Select(r => r).Any())
                {
                    stoppingCriteria = false;
                }
                else
                    stoppingCriteria = true;

                return stoppingCriteria;
            }


        }

        //returns index of closest cluster centroid
        private static int FindClosestClusterCenter(List<DocumentObjModel> clusterCenter, DocumentVectorSpaceModel obj)
        {

            float[] similarityMeasure = new float[clusterCenter.Count()];

            for (int i = 0; i < clusterCenter.Count(); i++)
            {

                similarityMeasure[i] = DistanceCalculation.FindCosineSimilarity(clusterCenter[i].GroupedDocument[0].VectorSpace, obj.VectorSpace);

            }

            int index = 0;
            float maxValue = similarityMeasure[0];
            for (int i = 0; i < similarityMeasure.Count(); i++)
            {
                //if document is similar assign the document to the lowest index cluster center to avoid the long loop
                if (similarityMeasure[i] > maxValue)
                {
                    maxValue = similarityMeasure[i];
                    index = i;

                }
            }
            return index;

        }

        //Reposition the centroid
        private static List<DocumentObjModel> CalculateMeanPoints(List<DocumentObjModel> _clusterCenter)
        {

            for (int i = 0; i < _clusterCenter.Count(); i++)
            {

                if (_clusterCenter[i].GroupedDocument.Count() > 0)
                {

                    for (int j = 0; j < _clusterCenter[i].GroupedDocument[0].VectorSpace.Count(); j++)
                    {
                        float total = 0;

                        foreach (DocumentVectorSpaceModel vSpace in _clusterCenter[i].GroupedDocument)
                        {

                            total += vSpace.VectorSpace[j];

                        }
                        //reassign new calculated mean on each cluster center, It indicates the reposition of centroid
                        _clusterCenter[i].GroupedDocument[0].VectorSpace[j] = total / _clusterCenter[i].GroupedDocument.Count();
                    }

                }

            }
            return _clusterCenter;
        }
        private static float CalculateMeanPointsOfCluster(DocumentObjModel cluster)
        {
            float meanValue = 0.0f;
            if (cluster.GroupedDocument.Count() > 0)
            {
                for (int j = 0; j < cluster.GroupedDocument[0].VectorSpace.Count(); j++)
                {
                    float total = 0;
                    foreach (DocumentVectorSpaceModel vSpace in cluster.GroupedDocument)
                    {
                        total += vSpace.VectorSpace[j];
                    }
                    meanValue = total / cluster.GroupedDocument.Count();
                }
            }
            return meanValue;
        }
    }
}
