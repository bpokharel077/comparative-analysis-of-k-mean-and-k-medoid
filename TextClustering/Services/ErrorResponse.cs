﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TextClustering.Services
{
    public class ErrorResponse
    {
        public bool IsErrror { get; set; }
        public string Message { get; set; }
    }
}
