﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TextClustering.Models
{
    public class FileUploadModel
    {

        [FileExtensions(Extensions = "txt", ErrorMessage = "All files must be of txt format")]
        public List<IFormFile> fileName { get; set; }
        public int k { get; set; }
    }
}
