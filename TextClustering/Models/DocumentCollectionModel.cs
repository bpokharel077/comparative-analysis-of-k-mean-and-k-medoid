﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TextClustering.Models
{
    public class DocumentCollectionModel
    {
        [Required(ErrorMessage = "You must provide text for text clustering")]
        public List<string> DocumentList { get; set; }
    }

}
